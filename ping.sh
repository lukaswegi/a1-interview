#!/bin/sh

TIMESTAMP=$(date +%Y-%m-%d_%H-%M-%S)

echo $TIMESTAMP &&
curl https://api.universallock.net:8080/api/ping
echo "\n"
