package at.a1.ping.util;


import java.time.Duration;
import java.time.LocalDateTime;

public class TimeUtil {
    public static LocalDateTime getCurrentTimeStamp(){
        return LocalDateTime.now();
    }

    public static Duration getResponseTime(LocalDateTime localDateTime) {
        return Duration.between(localDateTime, getCurrentTimeStamp());
    }

}
