package at.a1.ping.controller;

import at.a1.ping.dto.PingDTO;
import at.a1.ping.model.Ping;
import at.a1.ping.service.AutoIncrementService;
import at.a1.ping.service.PingService;
import at.a1.ping.util.TimeUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PingController {

    @Autowired
    private PingService pingService;

    @Autowired
    private AutoIncrementService autoIncrementService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(path = "/ping")
    public PingDTO createPing(){
        Ping ping = new Ping();
        ping.setResponseTime(TimeUtil.getResponseTime(ping.getTimeStamp()));
        ping.setId(autoIncrementService.generateSequence(Ping.SEQUENCE_NAME));
        pingService.addPing(ping);
        return convertToDto(ping);
    }

    @GetMapping(path = "/pings")
    public List<Ping> getAll(){
        return pingService.getAllPings();
    }

    private PingDTO convertToDto(Ping post) {
        return modelMapper.map(post, PingDTO.class);
    }



}
