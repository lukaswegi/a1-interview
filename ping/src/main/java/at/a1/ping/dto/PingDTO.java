package at.a1.ping.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class PingDTO {
    private Integer Id;
    private LocalDateTime timeStamp;
}
