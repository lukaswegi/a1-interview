package at.a1.ping.model;

import at.a1.ping.util.TimeUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Duration;
import java.time.LocalDateTime;

@Getter
@Setter
@Document
public class Ping {

    @Transient
    public static final String SEQUENCE_NAME = "ping_sequence";

    @Id
    private long Id;

    private LocalDateTime timeStamp;

    private Duration responseTime;

    public Ping() {
        setTimeStamp();
    }

    private void setTimeStamp(){
        this.timeStamp = TimeUtil.getCurrentTimeStamp();
    }
}
