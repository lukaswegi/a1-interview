package at.a1.ping.repository;

import at.a1.ping.model.Ping;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PingRepository extends MongoRepository<Ping, Integer> {
}
