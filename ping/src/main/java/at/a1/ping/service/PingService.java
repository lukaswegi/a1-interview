package at.a1.ping.service;

import at.a1.ping.model.Ping;
import at.a1.ping.repository.PingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PingService {

    @Autowired
    private PingRepository pingRepository;

    public void addPing(Ping ping){
        pingRepository.insert(ping);
    }

    public List<Ping> getAllPings(){
        return pingRepository.findAll();
    }

}
